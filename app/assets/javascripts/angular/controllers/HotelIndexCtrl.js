blackseaonline.controller("HotelIndexCtrl", ['$scope', '$http', '$resource', 'Hotels', 'Hotel', '$location', function($scope, $http, $resource, Hotels, Hotel, $location) {

  $scope.hotels = Hotels.query();

  $scope.viewHotel = function(id) {
  	return $location.url("/hotels/" + id); 
  };

}]);