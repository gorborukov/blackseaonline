var blackseaonline = angular.module('blackseaonline', ['ngRoute', 'ngResource']);

//Factory
blackseaonline.factory('Hotels', ['$resource',function($resource){
  return $resource('/hotels.json', {},{
    query: { method: 'GET', isArray: true },
    create: { method: 'POST' }
  })
}]);

blackseaonline.factory('Hotel', ['$resource', function($resource){
  return $resource('/hotels/:id.json', {}, {
    show: { method: 'GET' },
    update: { method: 'PUT', params: {id: '@id'} },
    delete: { method: 'DELETE', params: {id: '@id'} }
  });
}]);

blackseaonline.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
  $routeProvider.
  	when('/hotels', {
  		templateUrl: '../templates/hotels/index.html',
  		controller: 'HotelIndexCtrl'
  	}).
  	when('/hotels/:id', {
  		templateUrl: '../templates/hotels/show.html',
  		controller: 'HotelShowCtrl'
  	}).
    otherwise({
      templateUrl: '../templates/home.html',
      controller: 'HomeCtrl'
    }); 
}]);